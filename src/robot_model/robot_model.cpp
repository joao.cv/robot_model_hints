#include <fmt/core.h>
#include <umrob/robot_model.h>

#include <urdf_model/pose.h>
#include <urdf_parser/urdf_parser.h>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <stdexcept>

#include <iostream>

namespace umrob {

RobotModel::RobotModel(const std::string& urdf)
    // : model_{urdf::parseURDF(urdf)} 
    {

    std::cout << "\n\n--- Loading URDF C++ model ---\n\n";
    model_ = urdf::parseURDF(urdf);
    std::cout << "\n\n--- URDF C++ model loaded ---\n\n";

    // Object link
    urdf::LinkConstSharedPtr link;    
    // Object joint
    urdf::JointConstSharedPtr joint;    

    std::cout << "\n\n==========================================\n==========================================\n===    Showing URDF Parser features   ====\n==========================================\n==========================================\n\n";

    // Get root link 
    link = model_->getRoot();    

    // Link name
    std::cout << "  Name of the root link: " << link->name << "\n\n";


    // "link->child_joints" return a std::vector<JointConstSharedPtr> of every child joint of a link
    joint = link->child_joints[0];
    std::cout << "  Name of its first child joint:" <<  joint->name << "\n\n";

    // "joint->type" returns the type "urdf::Joint"  
    /// \brief     type_       meaning of axis_
    /// ------------------------------------------------------
    ///            UNKNOWN     unknown type
    ///            REVOLUTE    rotation axis
    ///            PRISMATIC   translation axis
    ///            FLOATING    N/A
    ///            PLANAR      plane normal axis
    ///            FIXED       N/A
    std::cout << "  Is this a CONTINUOUS joint (?): " << (joint->type == urdf::Joint::CONTINUOUS) << "\n\n";
    std::cout <<  "  Is this a PLANAR joint (?): " <<(joint->type == urdf::Joint::PLANAR) << "\n\n";

    // The std::map memebers related to the joints may be initialized, since we know each joint->name
    joints_position_[joint->name] = 0;
    // Same for joint limits, which belongs to type JointLimits
    JointLimits limits;
    if (joint->limits) {
        limits.min_position = joint->limits->lower;
        limits.max_position = joint->limits->upper;
        limits.max_velocity = joint->limits->velocity;
        limits.max_effort = joint->limits->effort;
    }
    joints_limits_[joint->name] = limits;

    // A joint has one (and only one) child link. It name can be accessed with joint->child_link_name
    // Method getlink(name) of a urdf model returns the link with name given by a string
    std::cout <<  "  The name of its child link is " << joint->child_link_name  << "\n\n";
    link = model_->getLink(joint->child_link_name);

    // The axis of a joint is obtained with joint->axis. Terms x, y and z, are given seperately
    std::cout << "  Joint axis: [" << joint->axis.x << ", " << joint->axis.y << ", " << joint->axis.z << "]\n\n";


    // The rigid body transformation of a joint from its parent joint is obtained with joint->parent_to_joint_origin_transform;
    // It returns an object of class Pose, which has, among others, the following members:
        // Vector3  position;
        // Rotation rotation;
    urdf::Pose origin = joint->parent_to_joint_origin_transform;
    // Class Vector3 has notably the members x, y and z
    // Class Rotation has notably the member function 
    //      void getQuaternion(double &quat_x,double &quat_y,double &quat_z, double &quat_w) const
    double qx, qy, qz, qw;
    origin.rotation.getQuaternion(qx,qy,qz,qw);

    
    // Eigen::AngleAxisd may be particularly usefull to generate a roration matrix based on an angle and an axis:
    Eigen::AngleAxisd(M_PI,Eigen::Vector3d(0,0,1) ).toRotationMatrix();

}


} // namespace umrob