#include "umrob/robot_model.h"

#include <fmt/format.h>
#include <fmt/ostream.h>

#include <fstream>
#include <sstream>
#include <filesystem>

#include <iostream>

//! \brief Look for a file named 'model_name'.urdf in the
//! 'share/robot-model/models' path next to 'exe_path'
//!
//! \param exe_path current executable path (typically argv[0])
//! \param model_name name of the model to load
//! \return std::string model description
std::string load_urdf(std::string_view exe_path, std::string_view model_name);

int main(int argc, char* argv[]) {
    // Load the URDF model from a file
    const std::string urdf = load_urdf(argv[0], "nao_arms");   

    // Construct a robot model with a given URDF description
    auto model = umrob::RobotModel(urdf);

}

std::string load_urdf(std::string_view exe_path, std::string_view model_name) {
    const auto binary_path = std::filesystem::path(exe_path).parent_path();
    const auto model_path =
        binary_path / std::filesystem::path(fmt::format(
                          "share/robot-model/models/{}.urdf", model_name));
    std::ifstream model_file(model_path.generic_string());
    if (not model_file.is_open()) {
        fmt::print(stderr, "Failed to open the {} URDF file\n", model_name);
        std::exit(-1);
    }

    return std::string{(std::istreambuf_iterator<char>(model_file)),
                       std::istreambuf_iterator<char>()};
}