#include "umrob/robot_model.h"

#include <fmt/format.h>
#include <fmt/ostream.h>
#include <iostream>

constexpr auto planar_robot =
    R"(<robot name="PlanarRobot">
    <link name="root" />
    <link name="b1" />
    <link name="b2" />
    <link name="tcp" />
    <joint name="j1" type="continuous">
        <parent link="root"/>
        <child link="b1"/>
        <origin rpy="0 0 0" xyz="0 0 0"/>
        <axis xyz="0 0 1"/>
    </joint>
    <joint name="j2" type="continuous">
        <parent link="b1"/>
        <child link="b2"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
        <axis xyz="0 0 1"/>
    </joint>
    <joint name="j3" type="fixed">
        <parent link="b2"/>
        <child link="tcp"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
    </joint>
</robot>
)";

int main(int argc, char* argv[]) {
    // Construct a robot model with a given URDF description
    auto model = umrob::RobotModel(planar_robot);
}
