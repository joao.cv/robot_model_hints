if(ENABLE_TESTING)
    # Compile Catch2 main function separately to share it across tests and speed up subsequent builds
    add_library(test_main STATIC common/main.cpp)
    target_link_libraries(test_main PUBLIC CONAN_PKG::catch2)

    # List all the source files composing the tests
    set(forward_kinematics_test_FILES 
        forward_kinematics/forward_kinematics.cpp
    )

    add_executable(forward_kinematics_test ${forward_kinematics_test_FILES})

    target_link_libraries(forward_kinematics_test PUBLIC test_main robot_model)

    add_test(NAME forward_kinematics-test COMMAND forward_kinematics_test)
endif()