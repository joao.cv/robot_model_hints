//! \file robot_model.h
//! \author Benjamin Navarro
//! \brief Declaration of the RobotModel class
//! \date 08-2020

#pragma once

#include <Eigen/Dense>
#include <urdf_parser/urdf_parser.h>

#include <limits>
#include <map>
#include <string>
#include <type_traits>

namespace umrob {

//! \brief Hold the position, velocity and effort limits of a joint
//!
struct JointLimits {
    //! \brief Minimum joint position (rad)
    double min_position{-std::numeric_limits<double>::infinity()};
    //! \brief Maximum joint position (rad)
    double max_position{std::numeric_limits<double>::infinity()};
    //! \brief Maximum joint velocity (rad/s)
    double max_velocity{std::numeric_limits<double>::infinity()};
    //! \brief Maximum joint effort (Nm)
    double max_effort{std::numeric_limits<double>::infinity()};
};

//! \brief Hold a 6xDofs Jacobian matrix and its associated list of joints
//!
struct Jacobian {
    //! \brief The Jacobian matrix
    Eigen::Matrix<double, 6, Eigen::Dynamic> matrix;
    //! \brief Names of the joints associated with each column of the matrix, in
    //! the same order
    std::vector<std::string> joints_name;
    //! \brief Joints involved in the Jacobian
    std::vector<urdf::JointConstSharedPtr> joints_chain;
};

//! \brief Handle computations related to a robot model: forward kinematics,
//! jacobian extraction, etc
//!
class RobotModel {
public:

    //! \brief Construct a RobotModel using a URDF description of the robot
    //!
    //! \param urdf model description in URDF format
    explicit RobotModel(const std::string& urdf);


private:
    urdf::ModelInterfaceSharedPtr model_;
    std::map<std::string, double> joints_position_;
    std::map<std::string, Eigen::Affine3d> links_pose_;
    std::map<std::string, JointLimits> joints_limits_;
    std::map<std::string, Jacobian> links_jacobian_;
    size_t degrees_of_freedom_{0};
};

} // namespace umrob